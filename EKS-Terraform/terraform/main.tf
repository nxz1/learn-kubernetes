provider "aws" {
    region =  "us-east-1"
    profile = "acg"
}

terraform {
  required_providers {
    aws = {
        source = "hashicorp/aws"
        version = "~> 3.0"
    }
  }
}

variable "environment" {
    type = string
    description = "Environment for eks cluster"
}

module "eks" {
    source = "./services/eks"
    env = "${var.environment}"
}

#module "cognito" {
#    source = "./services/cognito"
#}

output "IAM_TEST_ARN" {
    value = module.eks.test_policy_arn
}

output "AmazonEKS_EBS_CSI_DriverRole_ARN" {
    value = module.eks.EBS_CSI_DriverRole_Arn
}
