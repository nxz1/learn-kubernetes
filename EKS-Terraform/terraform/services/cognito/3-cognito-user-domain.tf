resource "aws_cognito_user_pool_domain" "cognito-domain" {
  domain       = "nerdcentra"
  user_pool_id = "${aws_cognito_user_pool.user_pool.id}"
}