data "aws_iam_policy_document" "ebs_csi_driver_policy" {
    statement {
        actions = ["sts:AssumeRoleWithWebIdentity"]
        effect = "Allow"

        condition {
            test = "StringEquals"
            variable = "${replace(aws_iam_openid_connect_provider.eks.url, "https://", "")}:sub"
            values = ["system:serviceaccount:kube-system:ebs-csi-controller-sa"]
        }

        principals {
            identifiers = [aws_iam_openid_connect_provider.eks.arn]
            type = "Federated"
        }
    }
}

resource "aws_iam_role" "ebs_csi_driver_role" {
    assume_role_policy = data.aws_iam_policy_document.ebs_csi_driver_policy.json
    name = "AmazonEKS_EBS_CSI_DriverRole"
}

resource "aws_iam_policy" "ebs_csi_driver_policy" {
    name = "AmazonEKS_EBS_CSI_Driver_Policy"
    policy = jsonencode({
        Statement = [{
            Action = [
                "ec2:AttachVolume",
                "ec2:CreateSnapshot",
                "ec2:CreateTags",
                "ec2:CreateVolume",
                "ec2:DeleteSnapshot",
                "ec2:DeleteTags",
                "ec2:DeleteVolume",
                "ec2:DescribeAvailabilityZones",
                "ec2:DescribeInstances",
                "ec2:DescribeSnapshots",
                "ec2:DescribeTags",
                "ec2:DescribeVolumes",
                "ec2:DescribeVolumesModifications",
                "ec2:DetachVolume",
                "ec2:ModifyVolume",
            ]
            Effect = "Allow"
            Resource = "*"
        }]
        "Version": "2012-10-17"
    })
}

resource "aws_iam_role_policy_attachment" "ebs_csi_driver_attach" {
    role = aws_iam_role.ebs_csi_driver_role.name
    policy_arn = aws_iam_policy.ebs_csi_driver_policy.arn
}

output "EBS_CSI_DriverRole_Arn" {
    value = aws_iam_role.ebs_csi_driver_role.arn
}