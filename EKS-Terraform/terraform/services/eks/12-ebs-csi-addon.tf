resource "aws_eks_addon" "example" {
  cluster_name = aws_eks_cluster.demo.name
  addon_name   = "aws-ebs-csi-driver"
  service_account_role_arn = aws_iam_role.ebs_csi_driver_role.arn
}