# Private Subnets (certain tags are required by eks to know where to put lb, place cluster on a vpc)
resource "aws_subnet" "private-us-east-1a" {
    vpc_id = aws_vpc.main.id
    cidr_block = "172.16.0.0/19"
    availability_zone = "us-east-1a"

    tags = {
        "Name" = "${var.env}-private-us-east-1a"
        "kubernetes.io/role/internal-elb" = "1"
        "kubernetes.io/cluster/${var.env}-eks-cluster" = "owned"
    }
}

resource "aws_subnet" "private-us-east-1b" {
    vpc_id = aws_vpc.main.id
    cidr_block = "172.16.32.0/19"
    availability_zone = "us-east-1b"

    tags = {
        "Name" = "${var.env}-private-us-east-1b"
        "kubernetes.io/role/internal-elb" = "1"
        "kubernetes.io/cluster/${var.env}-eks-cluster" = "owned"
    }
}

# Public Subnets (certain tags are required by eks to know where to put lb, place cluster on a vpc)
resource "aws_subnet" "public-us-east-1a" {
    vpc_id = aws_vpc.main.id
    cidr_block = "172.16.64.0/19"
    availability_zone = "us-east-1a"
    # only needed if you want to create public eks instance groups
    map_public_ip_on_launch = true # only applied for ec2 not elb

    tags = {
        "Name" = "${var.env}-public-us-east-1a"
        "kubernetes.io/role/elb" = "1" # required for eks 
        "kubernetes.io/cluster/${var.env}-eks-cluster" = "owned"
    }
}

resource "aws_subnet" "public-us-east-1b" {
    vpc_id = aws_vpc.main.id
    cidr_block = "172.16.96.0/19"
    availability_zone = "us-east-1b"
    # only needed if you want to create public eks instance groups
    map_public_ip_on_launch = true

    tags = {
        "Name" = "${var.env}-public-us-east-1b"
        "kubernetes.io/role/elb" = "1"
        "kubernetes.io/cluster/${var.env}-eks-cluster" = "owned"
    }
}