resource "aws_eip" "nat_eip" {
    vpc = true
    tags = {
        Name = "nat"
    }
}

resource "aws_nat_gateway" "nat" {
    allocation_id = aws_eip.nat_eip.id
    subnet_id = aws_subnet.public-us-east-1a.id

    tags = {
        Name = "${var.env}-nat-gw"
    }

    depends_on = [aws_internet_gateway.gw]
}