# IAM Role to use eks service
resource "aws_iam_role" "cluster_role" {
  name = "eks-cluster-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

# Attach a managed AWS policy to the role
resource "aws_iam_role_policy_attachment" "demo-AmazonEKSClusterPolicy" {
    policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
    role = aws_iam_role.cluster_role.name
}

# Create an EKS cluster with minimum settings
resource "aws_eks_cluster" "demo" {
    name = "${var.env}-eks-cluster"
    role_arn = aws_iam_role.cluster_role.arn

    vpc_config {
        subnet_ids = [
            aws_subnet.private-us-east-1a.id,
            aws_subnet.private-us-east-1b.id,
            aws_subnet.public-us-east-1a.id,
            aws_subnet.public-us-east-1b.id
        ]
    }

    depends_on = [aws_iam_role_policy_attachment.demo-AmazonEKSClusterPolicy]
}