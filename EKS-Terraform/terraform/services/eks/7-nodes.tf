# IAM Role for node group
resource "aws_iam_role" "workernode_role" {
    name = "eks-node-group-role"

    assume_role_policy = jsonencode({ # EXAMPLE OF TERRAFORM BUILT IN FUNCTION jsonencode
        Statement = [{
            Action = "sts:AssumeRole"
            Effect = "Allow"
            Principal = {
                Service = ["eks.amazonaws.com", "ec2.amazonaws.com"]
            }
        }]
        Version = "2012-10-17"
    })
}

# AmazonEKSWorkerNodePolicy: Grants access to ec2 and eks
resource "aws_iam_role_policy_attachment" "nodes-AmazonEKSWorkerNodePolicy" {
    policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
    role = aws_iam_role.workernode_role.name
}

# AmazonEKS_CNI_Policy: Allows pods network in VPC subnets
resource "aws_iam_role_policy_attachment" "nodes-AmazonEKS_CNI_Policy" {
    policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
    role = aws_iam_role.workernode_role.name
}

# AmazonEC2ContainerRegistryReadOnly: Allow Download and run image from ECR
resource "aws_iam_role_policy_attachment" "nodes-AmazonEC2ContainerRegistryReadOnly" {
    policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
    role = aws_iam_role.workernode_role.name
}

# Keypair
resource "aws_key_pair" "node_key" {
    key_name = "dev-key"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC6sP7c2oQNUzO6eKPOcfucKsmQqho+B+QhvQiKVynE7Fbz3fyGNp2UxBpEgjXK2G7WwUnEejnAWycSfRxgfn18h8q11GooeRY+sKtPcNeQWSocgXJRzZcZe11Q/X03PaycAM5eZcL0bHKQy96YZH6pmJtUuRzh6pHqghR8Z280b2RX4CkEfNXfxW77OccKzw8n/fe97NB19zEqWFo2fsDWV1PeiiWoE6qN44WIV0mtHR58nPa5LvvEYMl3QU5UOe+t3RQCQZtmtRDPg/32/fw6mxVnJBrqKSh+S5TI7ugkhJ29wxCKSjPfC4bH0KhqX4y83XvVLlRc3qvgcFq7G9KwIU24WEkgvV0CILq20Ijo1fYsjW7CELqiSALkFpKI5Ijge68Ln/+5CiMCZTuOAAEjbjQgbvxlCyJeyUkTwnZB2ouAObTaLGaX/HNbSW9gN7Fa3UsqX0cEpbRE6IIjWdvAyGKOh/wUDe2HCd7QaubRLVp73sTKRlpqgqv74TwsOdhcyH94o2GAyZLkKH7bR1viX2KwF9XVOg/VGLwnMohGb2WdeD/hTIX/iedg/MPXg5PEm7y6Z1P+4z1STkhtxMZ/oXHWIP3YOVDezVnBEK70IQ2FuR8pyjBmHjCUDYQ2gcyyhYjdMnrNP8ZmYWOj9gwBXnkb6A8s137Ox5eC3bQsHw== nawaz@ttn-laptop"
}

# Private Node Group 
resource "aws_eks_node_group" "private-nodes" {
    cluster_name = aws_eks_cluster.demo.name
    node_group_name = "private-nodes"
    node_role_arn = aws_iam_role.workernode_role.arn

    subnet_ids = [
        aws_subnet.private-us-east-1a.id,
        aws_subnet.private-us-east-1b.id
    ]

    capacity_type = "ON_DEMAND"
    instance_types = ["t3.small"]

# IMPORTANT: by default EKS will not autoscale your nodes, you need to deploy additional component -> Cluster Autoscaler
    scaling_config {
        desired_size = 1
        max_size = 6
        min_size = 0
    } # EKS will use this setting to create ASG on your behalf

    update_config {
        max_unavailable = 1
    }

    tags = {
        Name: "eks-private-node"
    }

    # Use labels to instruct kubernetes to use a particular node group while using affinity or node selector
    labels = {
        role = "general"
    }

    # taint {
    #     key = "team"
    #     value = "devops"
    #     effect = "NO_SCHEDULE"
    # }

    # launch_template {
    #     name = aws_launch_template.eks-with-disks.name
    #     version = aws_launch_template.eks-with-disks.latest_version
    # }

    depends_on = [
        aws_iam_role_policy_attachment.nodes-AmazonEKSWorkerNodePolicy,
        aws_iam_role_policy_attachment.nodes-AmazonEKS_CNI_Policy,
        aws_iam_role_policy_attachment.nodes-AmazonEC2ContainerRegistryReadOnly,
    ]

    remote_access {
        ec2_ssh_key = "dev-key"
    }
}

# resource "aws_launch_template" "eks-with-disks" {
#     name = "eks-with-disks"
#     key_name = "local-provisioner"

#     block_device_mappings {
#         device_name = "/dev/xvdb"

#         ebs {
#             volume_size = 50
#             volume_type = "gp2"
#         }
#     }
# }
