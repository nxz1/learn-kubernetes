variable "env" {
    type = string
    description = "Environment of the EKS cluster"
}