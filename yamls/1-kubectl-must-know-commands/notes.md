# Docker containers in kubernetes

#### 1. Create a busybox container
$ kubectl run myshell -it --image busybox -- sh

#### 2. Create a busybox container that terminates after exiting the shell
$ kubectl run myshell --rm -it --image busybox -- sh

#### 3. Create a nginx container
$ kubectl run nginx --image nginx

#### 4. Port forward (local port to container port)
$ kubectl port-forward nginx-container 8080:80

#### 5. Create a nginx deployment
$ kubectl create deployment nginx --image nginx

#### 6. Expose deployments (NodePort Service Type)
$ kubectl expose deployment nginx --type NodePort --port 80

#### 7. Scale deployments 
$ kubectl scale deployment nginx --replicas 4

#### 8. Exporting deployment to yaml
$ kubectl get deployment nginx -o yaml > /tmp/nginx.yaml

#### 9. Exporting service to yaml
$ kubectl get svc nginx -o yaml > /tmp/nginx-service.yaml
