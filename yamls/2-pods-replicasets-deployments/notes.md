## Pod:
	Pod is just a standalone application which exist by itself.
##### Command
	$ kubectl describe pod nginx-pod

## Replicaset:
	Replicaset enables you to always maintain a consistent number of pod to exist
	Even if you delete a pod, it will recreate it to maintain the state.
	It knows its pods, based on the labels that is attached to the pod.
	You can verify which pod is controlled by which replicaset by checking ControlledBy setting of the pod 

##### Command
	$ kubectl get pod -l run=nginx  
	$ kubectl describe rs nginx-replicaset
	$ kubectl describe rs nginx-replicaset | grep -i controlled

## Deployment:
	Deployments strategies are useful when you want to do rolling
	updates. It allows you maintain an x percent of service availability in your cluster. It knows its pods, based on the labels that is attached to the pod. 
	You can verify which pod is controlled by which replicaset by checking ControlledBy setting of the pod 
##### Command
	$ kubectl get pod -l run=nginx
	$ kubectl describe deploy nginx-deployment
	$ kubectl describe deploy nginx-deployment | grep -i controlled	

##### Scale the deployment and verify
	$ kubectl scale deploy nginx-deployment --replicas 5
	$ kubectl get deploy nginx-deployment && kubectl get pod -l run=nginx
