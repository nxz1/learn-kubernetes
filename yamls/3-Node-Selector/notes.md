## Node Selector is used when you want to run certain workloads on a specific type of hardware.

Think of an app like youtube which need more processing power (cpu) to render / process / compress the video in different formats to be available to the user. (480p / 720p / 1080p)

In such scenario, you need to schedule workloads on a specific type of hardware.

### STEPS:
##### 1. Label the node
    $ kubectl label node <node-name> model=m6.4xlarge

##### 2. In the container specification of the deployment, use nodeSelector option. 
NOTE: nodeSelector should be in container level.
    
    spec:
        containers:
        ..
        ..
        nodeSelector:
            model: "m6.4xlarge"

##### 3. Check the label
    $ kubectl get nodes --show-labels
    $ kubectl get nodes -l model=m6.4xlarge

##### 4. Apply the deployment
    $ kubectl apply -f nginx-deployment.yaml

##### 5. Check the nodes on which the pods are deployed
    $ k get pods -o wide

##### 6. Delete the pod and verify the workload is scheduled on the same node.
    $ kubectl delete pod <pod-name>