## Admission Controllers

### What are they?
It is a piece of code that intercepts the kubernetes API prior to the persistence of the object but after the request is authenticated and authorized

### Why do I need them?
Many advanced features in Kubernetes require an admission controller to be enabled in order to properly support the feature. As a result, a Kubernetes API server that is not properly configured with the right set of admission controllers is an incomplete server and will not support all the features you expect.

#### PodNodeSelector:
Forcing pods to run on specific labeled nodes
This admission controller defaults and limits what node selectors may be used within a namespace by reading a namespace annotation and a global configuration.

UseCase:
---
In this lesson, I will show you how to use PodNodeSelector admission controller plugin to assign pod that are deployed to a specific namespace to be scheduled on specific set of worker nodes.

Whenever a workload is scheduled on a specific namespace, it should be scheduling the workloads on a particular set of hardware.

Whenever i want to deploy any component (pod, replicaset, daemonset, deployment) on a particular hardware, I want it to schedule on a particular set of worker nodes.

One way to do is to use NodeSelector in your pod specification, but that would be a labourious work to change the podSpec on each of the pod.  

By using PodNodeSelector admission controller plugin, you can just do that by editing the namespace to only run tasks on nodes with a specific set of labels.

#### Example:
    Kmaster - Control Plane
    Kworker1 - worker node 1 
    Kworker2 - worker node 2	

##### STEP 1: Label the node
    $ kubectl label node kworker1 env=dev
    $ kubectl label node kworker2 env=prod

##### STEP 2: Enable the PodNodeSelector admission controller plugin in k8s cluster
    $ watch -n1 'kubectl get pods -n kube-system'
In another tty session
    
    $ ssh root@kmaster.example.com
    $ sudo vim /etc/kubernetes/manifests/kube-apiserver.yaml
Append PodNodeSelector to the admission plugin

    --enable-admission-plugin= PodNodeSelector
This will restart the api server, so you will temporarily loose connection to the cluster.

##### Step 3: Once api server is up, edit the namespace
    $ kubectl edit ns dev
Add an annotations

    ..
    metadata:
        annotations:
            scheduler.alpha.kubernetes.io/node-selector: "env=dev"

##### Step 4: Now create a deployment.
    $ kubectl create deployment nginx --image nginx -n dev
    $ watch -n1 'kubectl get all -o wide -n dev'

##### Verify all the workloads are scheduled in kworker1
    $ kubectl get pods -n dev -o wide

##### Delete a pod and check if it's scheduled again on the same node.
    $ kubectl delete pod <pod-name> -n dev
    $ kubectl get pods -n dev

### Task: Do the same for prod namespace.