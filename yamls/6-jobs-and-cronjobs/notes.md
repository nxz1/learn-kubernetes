Jobs and Cronjobs
---

### Jobs
##### Default run

##### Killing pod restarts pod
    Job runs as long as the job get a successful exit code.
    
##### completions
    Job completes x time and ends
    
##### parallelism
    How many jobs to run in parallel

##### backofflimit
    How much retries.
    if you say 3, then after 3 tries, kubernetes will try it a 4th time and backsoff
    
##### activeDeadlineseconds
    How long does a job should run.

##### Cronjobs: deleting cronjobs also deletes the pod (kubernetes v1.22.0)
##### Default run

##### Cron wiki @hourly, @daily, @monthly

##### Deleting cronjobs

##### successfulJobsHistoryLimit
    How many successful job history you want to maintain, default is 3.
    
##### failedJobsHistoryLimit
    How many failed job history you want to maintain, default is 1.
    
##### suspending cron jobs (kubectl apply, patch)
    In order to suspend a cronjob, change the suspend parameter under specification to true.
    You can also do it in the command line by using kubectl patch command
    $ kubectl patch cronjob <cronjob-name> -p '{spec:{"suspend":false}}'

##### concurrentPolicy (Allow, Forbid & Replace)
    Do you want a job when a job is already running, by default it is Allow.
    Make sure the code must cope up with this behaviour.
    If it doesn't, choose Replace. It will terminate the misbehaving(taking longer time than defined) job and starts new job.

##### idempotency

### Usecases:
mysql backups
sending emails
any backups
checking out sources periodically
