# TTL Controllers for finished resources

link to the documentation:   [TTLAfterFinished](https://v1-22.docs.kubernetes.io/docs/concepts/workloads/controllers/ttlafterfinished/)

### TTLAfterFinished Controller
The TTL controller only supports Jobs for now. A cluster operator can use this feature to clean up finished Jobs (either Complete or Failed) automatically by specifying the .spec.ttlSecondsAfterFinished field of a Job

### TTLAfterFinished controller
TTLAfterFinished controller is enabled by default on kubernetes v1.24.0
It's a good practice to manually enable any features we want to use.

### Enable feature gate in kube-apiserver and kube-controller-manager
Login to master node
    
    $ ssh root@172.16.16.100

Edit the kube-api-server manifest
    
    $ vim /etc/kubernetes/manifests/kube-api-server.yaml

Append the feature gate parameter to the kube-apiserver command

    kube-apiserver
    ..
    ..
    --feature-gates=TTLAfterFinished=true
    ..

    :wq

Append the feature gate parameter to the kube-controller-manager command

    kube-controller-manager
    ..
    ..
    --feature-gates=TTLAfterFinished=true
    ..

    :wq

### Verify if the jobs are deleted automatically after the TTL specified

The job should have ttlSecondsAfterFinished option to be set in order for the TTLAfterFinished feature gate to work.

Open the job manifest

    $ vim job.yaml

    apiVersion: batch/v1
    ..
    ..
    spec:
      ..
      ..
      ttlSecondsAfterFinished: 20


