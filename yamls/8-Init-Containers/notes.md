### Understanding init containers
A Pod can have multiple containers running apps within it, but it can also have one or more init containers, which are run before the app containers are started.

Init containers are exactly like regular containers, except:

Init containers always run to completion.
Each init container must complete successfully before the next one starts.

If a Pod's init container fails, the kubelet repeatedly restarts that init container until it succeeds. However, if the Pod has a restartPolicy of Never, and an init container fails during startup of that Pod, Kubernetes treats the overall Pod as failed.

To specify an init container for a Pod, add the initContainers field into the Pod specification

### Using init containers

##### init containers can be used for a variety of usecases
- git checkout
- mysql backup
- making the image small and light
- perform builds, etc

##### In this example, we will create a hello kubernetes index.html file and using shared volume (emptyDir - temporary directory to share pod lifecycle) we will mount it to nginx.

The pod specification has 3 sections
- volumes
- initContainers
- containers

Check out the yaml file.
