[Configure a persistent volume storage](https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/)

#### What is a persistent volume?
- A PersistentVolume (PV) is a piece of storage in the cluster that has been provisioned by an administrator or dynamically provisioned using Storage Classes. 
- It is a resource in the cluster just like a node is a cluster resource. 
- PVs are volume plugins like Volumes, but have a lifecycle independent of any individual Pod that uses the PV. 
- This API object captures the details of the implementation of the storage, be that NFS, iSCSI, or a cloud-provider-specific storage system.

#### What is a persistent volume claim?
- A PersistentVolumeClaim (PVC) is a request for storage by a user. 
- It is similar to a Pod. Pods consume node resources and PVCs consume PV resources. 
- Pods can request specific levels of resources (CPU and Memory). 
- Claims can request specific size and access modes (e.g., they can be mounted ReadWriteOnce, ReadOnlyMany or ReadWriteMany)

### Persistent Volume Lab using hostpath
In this lab, we will create a persistent volume, persistent volume claim and a deployment that will use the persistent volume

##### Create a persistent volume

    $ kubectl apply -f 1-nginx-pv.yaml

It will create a persistent volume or pv with name nginx, capacity of 1GB, Access mode of Read Write Once, and reclaim policy defaults to Retain.

    $ kubectl get pv
    $ kubectl describe pv <pv-name>


##### Create a persistent volume claim

    $ kubectl apply -f 2-nginx-pv-claim.yaml

The persistent volume claim will check the name of the pv, storage class name, access modes and based on that set the status to bound or pending

    $ kubectl get pvc
    $ kubectl describe pvc <pvc-name>

* Task for self
 AccessMode in pv is ReadWriteOnce, Try changing AccessMode to ReadWriteMany in pvc and investigate what happens

##### Create a deployment

    $ kubectl apply -f 3-nginx-deployment.yaml

This deployment will create 2 nginx replicas on 2 of the worker nodes.
The pv will create a directory "/tmp/nginx-data" on both nodes(kworker1 and kworker2 if you are following my vagrant setup)

##### Persistent Volume Reclaim Policy
### Note: PersistentVolumeReclaimPolicy on hostpath can only delete paths from /tmp. So if your pv is in any other path than /tmp, and when you delete pvc, the pv won't be deleted (kubectl describe pv)
- by default the persistent volume reclaim policy is set to retain. That is really useful when you want to debug some logs, or want to troubleshoot something.
- You can also change the reclaim policy to Delete.
- So whenever, the persistent volume claim (pvc) is deleted, the pv will also be deleted

### Change the PersistentVolumeReclaimPolicy to Delete and investigate what happens